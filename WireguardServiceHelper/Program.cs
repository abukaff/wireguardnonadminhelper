﻿// See https://aka.ms/new-console-template for more information
using Topshelf;
using WireguardServiceHelper;

// var webAppBuilder = WebApplication.CreateBuilder();
// var app = webAppBuilder.Build();
//
// app.MapGet("/", () => "Works");
// app.MapGet("/CheckTunnelStatus/{tunnelName}", Functions.CheckTunnelStatus);
// app.MapGet("/GetLocalConfigsNames", Functions.GetLocalConfigsNames);
// app.MapGet("/ConnectTunnel/{tunnelName}", Functions.ConnectTunnel);
// app.MapGet("/DisconnectTunnel/{tunnelName}", Functions.DisconnectTunnel);
//
// app.Run("http://localhost:6054");

HostFactory.Run(x =>
{
    x.Service<WebService>(s =>
    {
        s.ConstructUsing(name => new WebService());
        s.WhenStarted(tc => tc.Start());
        s.WhenStopped(tc => tc.Stop());

    });
    x.RunAsLocalSystem();
    x.SetDescription("Wireguard Helper service for non admin");
    x.SetDisplayName("Wireguard Helper");
    x.SetServiceName("WGHelper");
});




