﻿using System.Diagnostics;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using SharedClasses;

namespace WireguardServiceHelper;

public static class Functions
{
    private static string wireguardPath = "c:\\Program Files\\WireGuard\\wireguard.exe";
    private static string appPath = AppContext.BaseDirectory;

    public static List<string> GetLocalConfigsNames()
    {
        List<string> filesName = new List<string>();
        try
        {
            var ext = new List<string> {"conf"};
            var myFiles = Directory
                .EnumerateFiles(appPath, "*.*", SearchOption.AllDirectories)
                .Where(s => ext.Contains(Path.GetExtension(s).TrimStart('.').ToLowerInvariant()));
            foreach (var file in myFiles)
            {
                var filename = Path.GetFileNameWithoutExtension(file);
                if (!CheckHasSpecialCharacters(filename))
                    filesName.Add(filename);
            }
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return filesName;
    }

    public static bool CheckTunnelStatus(string tunnelName)
    {
        bool result = false;

        try
        {
            CheckHasSpecialCharactersThrowException(tunnelName);
            //string command= $"powershell Get-NetAdapter -Name \"{tunnelName}\"";
            string command = $"powershell \"Get-NetAdapter -Name \"{tunnelName}\"| Format-List -Property \"status\"";
            string output = ExecuteCMDCommand(command);
            result = !output.Contains("ObjectNotFound");
            // double check if tunnel is up by parsing "Status Word" too lazy to think of a better way :)
            if (result)
                result = output.Contains("Up");
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return result;
    }

    public static bool ConnectTunnel(string tunnelName)
    {
        try
        {
            CheckHasSpecialCharactersThrowException(tunnelName);
            string command = $"\"{wireguardPath}\" /installtunnelservice \"{appPath}\\{tunnelName}.conf\"";
            ExecuteCMDCommand(command);
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return CheckTunnelStatus(tunnelName);
    }

    public static bool DisconnectTunnel(string tunnelName)
    {
        try
        {
            CheckHasSpecialCharactersThrowException(tunnelName);
            string command = $"\"{wireguardPath}\" /uninstalltunnelservice {tunnelName}";
            ExecuteCMDCommand(command);
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return CheckTunnelStatus(tunnelName);
    }

    public static string GetTransferredBytes(string tunnelName)
    {
        string result = "";
        try
        {
            CheckHasSpecialCharactersThrowException(tunnelName);
            TransferredBytes transferredBytes = new TransferredBytes();
            string output =
                ExecuteCMDCommand(
                    $"powershell \"Get-Date | Format-List -Property \"DateTime\"; Get-NetAdapterStatistics -name {tunnelName} | Format-List -Property \"ReceivedBytes,SentBytes\"");
            output = output.Replace("\r", "");
            List<string> outputList = output.Split('\n').ToList();

            string[] dateTimeOutput = outputList.Find(x => x.StartsWith("DateTime")).Split(new[] {':'}, 2);
            string?[] receivedBytesOutput = outputList.Find(x => x.StartsWith("ReceivedBytes"))?.Split(":", 2);
            string?[] sentBytesOutput = outputList.Find(x => x.StartsWith("SentBytes"))?.Split(":", 2);

            transferredBytes.date = DateTime.Parse(dateTimeOutput[1]);
            transferredBytes.bytesReceived = float.Parse(receivedBytesOutput[1]);
            transferredBytes.bytesSent = float.Parse(sentBytesOutput[1]);
            result = JsonConvert.SerializeObject(transferredBytes);
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return result;
    }

    static string ExecuteCMDCommand(string command)
    {
        string result = "";
        try
        {
            Process cmd = new Process();
            cmd.StartInfo.FileName = "cmd.exe";
            cmd.StartInfo.RedirectStandardInput = true;
            cmd.StartInfo.RedirectStandardOutput = true;
            cmd.StartInfo.CreateNoWindow = true;
            cmd.StartInfo.UseShellExecute = false;
            cmd.Start();
            cmd.StandardInput.WriteLine(command);
            cmd.StandardInput.Flush();
            cmd.StandardInput.Close();
            cmd.WaitForExit();
            result = cmd.StandardOutput.ReadToEnd();
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return result;
    }

    static bool CheckHasSpecialCharacters(string tunnelName)
    {
        bool hasSpecialCharacter = true;
        Regex rgx = new Regex("[^A-Za-z0-9]");
        hasSpecialCharacter = rgx.IsMatch(tunnelName) || tunnelName.Contains(" ");
        return hasSpecialCharacter;
    }

    static void CheckHasSpecialCharactersThrowException(string tunnelName)
    {
        bool hasSpecialCharacter = CheckHasSpecialCharacters(tunnelName);
        if (hasSpecialCharacter)
            throw new Exception($"Tunnel Name has special characters : {tunnelName}");
    }
}