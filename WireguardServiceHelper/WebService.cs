﻿namespace WireguardServiceHelper;

public class WebService
{
    private WebApplication app;
    public void Start()
    {
        var webAppBuilder = WebApplication.CreateBuilder();
        app = webAppBuilder.Build();

        app.MapGet("/", () => "Works");
        app.MapGet("/CheckTunnelStatus/{tunnelName}", Functions.CheckTunnelStatus);
        app.MapGet("/GetLocalConfigsNames", Functions.GetLocalConfigsNames);
        app.MapGet("/ConnectTunnel/{tunnelName}", Functions.ConnectTunnel);
        app.MapGet("/DisconnectTunnel/{tunnelName}", Functions.DisconnectTunnel);
        app.MapGet("/GetTransferredBytes/{tunnelName}", Functions.GetTransferredBytes);

        app.RunAsync("http://localhost:6054");
    }
    
    

    public async void Stop()
    {
        await app.DisposeAsync();
        await app.StopAsync();
    }
}