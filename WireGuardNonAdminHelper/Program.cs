using MudBlazor.Services;
using Photino.Blazor;
using WireGuardNonAdminHelper;

class Program
{
    [STAThread]
    static void Main(string[] args)
    {
        var appBuilder = PhotinoBlazorAppBuilder.CreateDefault(args);

        appBuilder.Services
            .AddLogging();

        // register root component and selector
        appBuilder.RootComponents.Add<App>("app");
        appBuilder.Services.AddMudServices();
        var app = appBuilder.Build();

        app.MainWindow.SetTitle("WG Helper")
            .SetIconFile("Others/WireGuard-Icon-helper.ico")
            .SetUseOsDefaultSize(false)
            .SetSize(800, 500)
            .SetResizable(true)
            .Center();

        AppDomain.CurrentDomain.UnhandledException += (sender, error) =>
        {
            app.MainWindow.OpenAlertWindow("Fatal exception", error.ExceptionObject.ToString());
        };
 
        app.Run();


    }
}