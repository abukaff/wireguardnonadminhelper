﻿using Newtonsoft.Json;
using SharedClasses;

namespace WireGuardNonAdminHelper.Functions;

public static class HttpFunctions
{
    static HttpClient _httpClient = new HttpClient{BaseAddress = new Uri("http://localhost:6054")};

    public static async Task<bool> CheckTunnelStatus(string tunnelName)
    {
        
        bool result = false;
        try
        {
            result = await _httpClient.GetFromJsonAsync<bool>($"/CheckTunnelStatus/{tunnelName}");
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }
        
        return result;
    }

    public static async Task<TransferredBytes> GetTransferredBytes(string tunnelName)
    {
        TransferredBytes result = new TransferredBytes();
        try
        {
            var response  = await _httpClient.GetAsync($"/GetTransferredBytes/{tunnelName}");
            var responseResult = response.Content.ReadAsStringAsync().Result;
            result = JsonConvert.DeserializeObject<TransferredBytes>(responseResult);
        }catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

        return result;

    }
    
    public static async Task<List<string>> GetLocalConfigsNames()
    {
        
        List<string> result = new List<string>();
        try
        {
            result = await _httpClient.GetFromJsonAsync<List<string>>($"/GetLocalConfigsNames");
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }
        
        return result;
    }
    
    public static async Task ConnectTunnel(string tunnelName)
    {
        try
        {
            await _httpClient.GetAsync($"/ConnectTunnel/{tunnelName}");
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

    }
    
    public static async Task DisconnectTunnel(string tunnelName)
    {
        try
        {
            await _httpClient.GetAsync($"/DisconnectTunnel/{tunnelName}");
        }
        catch (Exception err)
        {
            string functionName = System.Reflection.MethodBase.GetCurrentMethod().Name;
            Logger.Log(functionName, err.Message);
        }

    }
    
}