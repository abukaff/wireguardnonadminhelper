﻿using MudBlazor;
using WireGuardNonAdminHelper.Dialogs;

namespace WireGuardNonAdminHelper.Functions;

public class DialogHelperFunctions
{
    
    private IDialogService _mudDialog;

    private IDialogReference _dialogReference;
    public DialogHelperFunctions(IDialogService dialogService)
    {
        _mudDialog = dialogService;
    }
    public async Task ShowProgressDialog()
    {
        DialogOptions dialogOptions = new DialogOptions
        {
            DisableBackdropClick = true,
            CloseOnEscapeKey = false
        };
        DialogParameters parameters = new DialogParameters();
        _dialogReference =  _mudDialog.Show<ProgressDialog>("Please wait",parameters,dialogOptions);
    }
    public async Task CloseProgressDialog()
    {
        _dialogReference.Close(null);
        
    }

    public async Task ShowInformationDialog(string Message, bool isError=false)
    {
        DialogOptions dialogOptions = new DialogOptions
        {
            DisableBackdropClick = true,
            CloseOnEscapeKey = true,
            NoHeader = true
        };
        DialogParameters parameters = new DialogParameters();
        parameters.Add("Message",Message);
        parameters.Add("isError",isError);
        _dialogReference =  _mudDialog.Show<InformationDialog>("",parameters,dialogOptions);
        await _dialogReference.Result;
    }

    public async Task ShowInformationDialog(List<string> MessageList,bool isError=false)
    {
        DialogOptions dialogOptions = new DialogOptions
        {
            DisableBackdropClick = true,
            CloseOnEscapeKey = true,
            NoHeader = true
        };
        DialogParameters parameters = new DialogParameters();
        parameters.Add("MessageList",MessageList);
        parameters.Add("isError",isError);
        _dialogReference =  _mudDialog.Show<ProgressDialog>("",parameters,dialogOptions);
    }

    public async Task<bool> ShowConfirmationDialog(string message)
    {
        bool result = false;
        DialogOptions dialogOptions = new DialogOptions
        {
            NoHeader = true
        };
        DialogParameters parameters = new DialogParameters();
        parameters.Add("Message",message);
        _dialogReference =  _mudDialog.Show<ConfirmationDialog>("",parameters,dialogOptions);
        var res = await _dialogReference.Result;
        if (res.Data != null)
            result = bool.Parse(res.Data.ToString());
        return result;
    }

}