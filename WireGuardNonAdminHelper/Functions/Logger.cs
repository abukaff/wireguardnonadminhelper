﻿namespace WireGuardNonAdminHelper.Functions;

public class Logger
{
    public static void Log(string Message)
    {
        Console.WriteLine(Message);
    }
    public static void Log(string FunctionName,string Message)
    {
        Console.WriteLine(FunctionName+":\n"+Message);
    }
}