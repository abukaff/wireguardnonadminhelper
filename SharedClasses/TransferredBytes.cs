﻿namespace SharedClasses;

public class TransferredBytes
{
    public DateTime date;
    public float bytesReceived;
    public float bytesSent;
}