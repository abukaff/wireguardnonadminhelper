# Wireguad Non Admin Helper
![visitors](https://visitor-badge.deta.dev/badge?page_id=abukaff.WireguardNonAdminHelper&left_color=#4A5454&right_color=#5391C9)

[[_TOC_]]

## What is it ?
A helper tool for IT admin to allow running Wireguard on userspace as the offical wireguard client requires admin previlages and editing the registery to start or stop the tunnel

The solution is made up of two parts
1. a small webserver running as service that can execute wireguard commands
2. a gui that can be used in user space without admin rights

Note : Administrator access is required for initial setup only (or updates)

## How to Install/Uninstall
Requirements
* Wireguard client installed on client machine
  * tip : to install on user account without getting "require admin to install error"
  * Run CMD as admin, Navigate to wireguard msi installation file and run <span style="color:Red">make sure to replace the msi name</span>
```
MsiExec.exe /i wireguard-arch-x.y.z.msi DO_NOT_LAUNCH=1 /qn
```
  * https://download.wireguard.com/windows-client/

Binaries
* [Release](https://gitlab.com/abukaff/wireguardnonadminhelper/-/releases)
* Contained in file name means its "self contained" with the runtime included making the size larger
* Use the otherone if .net6 runtime/sdk is already installed on client PC 

### Auto installation
1. Download the service and place it in a location of your desire for example "C:\Program Files\WGHelper"
2. Service : Run Install.bat as admin

### Manual installation
1. Download the service and place it in a location of your desire for example "C:\Program Files\WGHelper"
2. Install the service by <span style="color:Red"> **running command prompt as Administrator**</span>
3. Download the GUI application and place it in a location of your desire and maybe create a shortcut for the user on the desktop

Install the service
```bash
<Path>\WireguardServiceHelper.exe install
```
Start the service
```bash
net start WGHelper
```


### Auto Uninstall
Note : removing the helper does not shut down the tunnel, you are require to do it manually

Service : Run Uninstall.bat as admin

### Manual Uninstall
Note : removing the helper does not shut down the tunnel, you are require to do it manually
1. <span style="color:Red">**Run command prompt as Administrator**</span>
2. Navigate to the service location for example "C:\Program Files\WGHelper"

```bash
<Path>\WireguardServiceHelper.exe uninstall
```
## How to use it
1. Place the configuration files in the same directory as the service
2. The service functionality as of this version highly depends on the file name
   1. File name and make sure it ends with .conf (Spaces and special Characters are not allowed)
   2. Make sure to disconnect the tunnel before changing the configuration file name otherwise the service will not see the running connection
```
-----WGServiceHelperfolder
                  |- vpn1.conf
                  |- vpn2.conf
```



